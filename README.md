# EVPN to the host 

This repository holds the documentation, diagrams and configuration/virtual setup to integrate EVPN on hosts with Openstack or plain KVM.


Table of Contents
=================
* [Introduction](#introduction)
* [EVPN to the host](#evpn-to-the-host)
  * [Host Architecture](#host-architecture)
  * [Traffic flows](#traffic-flows)
  * [Host configuration](#host-configuration)
* [Integrating with Openstack](#integrating-with-openstack)
* [PoC environment](#poc-environment)
* [Future work](#future-work)


Introduction
------------------------
In a typical Openstack / VM deployment scenario an overlay network is built between hypervisors to allow for multi-tenancy. A common scenario is displayed in the following diagram:

![OS-networking](diagrams/OS-networking.png) 

This deployment scenario has multiple caveats:

* The Physical underlay network infrastructure is ignored. If a physical host needs to be added to the tenant network plugins like OVSDB, ML2 or OVN need to be used. This would create an issue for deployments, because it will end up in exceptional scenarios which are harder to automate in the network infrastructure. Network vendors have to support these features when they change over time. 
* Traffic to/from a VM is routed through a network node which could cause congestion, traffic tromboning and additional latency
* Connections to a hypervisor are typically realised with multi-chassis LAG. MLAG is not an open standard and each vendor has it's own implementation. This locks a connection to a single vendor and MLAG also has it's caveats.
* Namespaces for tenant separation is not seen as a scalable solution.

In general when looking at Openstack networking from the perspective of a network engineer, it is hard to comprehend and can be improved by using standards and protocols that are common in datacenter networking.

EVPN-VxLAN (RFC8365) with additions for routing (draft-sajassi-l2vpn-evpn-inter-subnet-forwarding, https://tools.ietf.org/html/draft-ietf-bess-evpn-prefix-advertisement-11) has become the standard for overlay networking and multi tenancy. When looking at the functionalities offered in Openstack networking, EVPN-VxLAN can provide the same functionalities and when brought to the host have added value in a distributed setup.

EVPN to the host
------------------------

### Host Architecture

While EVPN is currently used in the domain of the physical infrastructure, we can provide this on a host with the current available tools and software. We will use the following to implement EVPN on a host:

* FreeRangeRouting, a routing suite with BGP-EVPN support, https://frrouting.org 
* >= 4.17 Linux kernel, this includes support for VRFs, ARP/ND suppression (needed for EVPN-VxLAN).
* Linux VRF tools, https://github.com/CumulusNetworks/vrf
* iproute2 >= 4.14
* ifupdown2 as network interface manager, https://github.com/CumulusNetworks/ifupdown2

When using these tools togehter, a hypervisor setup will look as in the following diagram:

![OS-with-EVPN](diagrams/OS-with-EVPN.png)

* A VM will be provisioned with its interface connected to a veth interface. 
* This veth interface will be a member of a vlan reserved for that tenant (a tenant can have multiple vlan interfaces and thus L2domains if necessary).
* Vlans are provided by a vlan-aware bridge (https://vincent.bernat.im/en/blog/2017-linux-bridge-isolation).
* Each vlan will have a corresponding L2VNI (VxLAN tunnel) configured. This allows L2 connectivity between VMs on different hosts. 
* If a tenant L2domain needs to have routing functionality, an SVI with an anycast gateway will be configured for this vlan. 
* To provide L3 multitenancy a tenant VRF with a corresponding L3VNI has to be configured.
* FRR will advertised the configured L2VNIs, L3VNIs and configured prefixes.

### Traffic flows

With the aforementioned architecture, there are multiple traffic flow "scenarios" possible for tenant VMs. 

* Intra tenant L2 connectivity on the same hypervisor

If VMs are deployed on the same hypervisor the VM will send out an ethernet frame out of the virtual interface. This is received on the hypervisors vlan (a veth interface inside the vlan-aware bridge) that has learned the mac address of both VMs through normal L2 learning. This frame is forwarded out of the veth interface where the mac address of the other VM is learned.

![Intra L2 Local](diagrams/intra-l2-local.png)

* Intra tenant L3 connectivity on the same hypervisor

If VMs from the same tenant (same VRF) are deployed on the same hypervisor, a VM will send the ethernet frame to its configured gateway (the SVI for the vlan). The system has learned the local routes and mac addresses which allows it to forward it to the other VM in the other vlan.

![Intra L3 Local](diagrams/intra-l3-local.png)

* Intra tenant L2 connectivity between different hypervisors

When VMs are deployed on different hypervisors, the EVPN functionalities are needed. EVPN exchanges type 2 and 3 messages over the spine/leaf architecture. This allows the hypervisors to advertise the configured VNIs (type 3) and the locally learned mac addresses (type 2). If a VM sends sends an ethernet frame, the kernel has learned the remote mac on the configured Vxlan tunnel. It will encapsulate the frame in a VxLAN header and forward it to the remote host. This will decapsulate the frame and send it out of the veth interface, simlar to local forwarding. From a VM perspective, there is no difference on the functionality or visibility if a VM is local or on a remote hypervisor.

![Intra L2 Remote](diagrams/intra-l2-remote.png)

* Intra tenant L3 connectivity between different hypervisors

When VMs need L3 connectivity beyond the local hypervisor, host routes and/or prefix routes are exchanged with type2/5 messages in EVPN. Forwarding of IP-traffic happens over a configured L3VNIs. This VNI is advertised with type3 messages, just like the regular L2VNIs. If a VM sends an ip packet to the SVI like in the previous local scenario, the destination is learned via EVPN. The next-hop is the local L3VNI and the packet will be encapsulated in the VxLAN header and send to the destination. The process on the receiving VTEP is the reverse and allows VMs in the same tenant to have L3 connectivity. Like with remote L2 connections, there is no difference for the VM itself. 

![Intra L3 Remote](diagrams/intra-l3-remote.png)

* Inter tenant L3 connectivity

Routes between tenants are not shared (separate VRFs). This means that routing between tenants has to go through an external router (router on a stick). While this isn't the most efficient traffic flow, in most cases VMs of different tenants wouldn't be directly communicating, this would be with clients outside the network or for security reasons through a firewall device. It is possible to do route leaking between VRFs, this functionality hasn't been implemented in FRR at this time (see future work).


### Host configuration

To test the functionality, hosts were configured with namespaces to "simulate" a VM.


Openstack integration
------------------------

KVM integration
------------------------


PoC environment
------------------------
As PoC environment, we use the Cumulus Networks [cl-piat](https://github.com/CumulusNetworks/cl-piat) demo topology. Follow the instructions to setup the demo, however, this repository includes different Ansible playbooks. Instead of deploying the playbooks from the cl-piat repo, use the playbooks that are included in the poc-environment directory in this repository.

The demo topology is a two-pod leaf/spine network configured with EVPN. This would allow hosts to be configured with EVPN and Openstack as well and show integration with "physical" hosts and VMs running on hypervisors without plugins such as ML2 or OVSDB. Servers 1-4 are configured as Openstack / VM hosts, servers 5 and 6 are deployed as regular hosts. External routes are being announced from the rtr01 and rtr02 devices, these are also used to show inter tenant communication.


Future work
------------------------

* Currently Headend replication is being used for flooding BUM packets. This limits scalability on merchant silicon. Work is being done for FRR to solve this by not using HER at all or use multicast for BUM flooding.
* Recently (kernel >= 4.17) functionalities for BPfilter (packet filtering using BPF, https://linux-audit.com/bpfilter-next-generation-linux-firewall/) was added. This would allow for having distributed firewall functionalities in the aforementioned setup.
* Features like NAT and Loadbalancing are not part of this project. Direct connectivity to VMs is assumed.
* FRR currently doesn't support route-leaking, this would allow for services vlans and external routing. Currently those routes have to be injected using a VRF-lite setup
